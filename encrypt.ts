import CryptoJS from 'crypto-js'
import fs from 'fs'
import path from 'path'

if (!process.env.SECRET) {
  console.error('Error: SECRET required\n')
  process.exit(1)
}

const SECRET = process.env.SECRET!

function main() {
  const data = fs.readFileSync(path.resolve(__dirname, 'data/profile.json'))
  const encrypted = CryptoJS.AES.encrypt(data.toString(), SECRET)

  fs.mkdirSync(path.resolve(__dirname, 'dist/vault'), { recursive: true })
  fs.writeFileSync(path.resolve(__dirname, 'dist/vault/profile.enc.json'), JSON.stringify({
    data: encrypted.toString(),
  }))
}

main()
