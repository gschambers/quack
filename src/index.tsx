import 'reflect-metadata'

import React from 'react'
import ReactDOM from 'react-dom'
import { App } from './components/App'
import { bootstrapContainer, ContainerContext } from './ContainerContext'

import './index.css'

const container = bootstrapContainer()

ReactDOM.render(
  <ContainerContext.Provider value={container}>
    <App />
  </ContainerContext.Provider>,
  document.querySelector('#root')
)
