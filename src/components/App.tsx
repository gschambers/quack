import React, { useEffect, useState } from 'react'
import { useInstance } from '../hooks/useInstance'
import { AuthService } from '../services/AuthService'
import { ProfileService } from '../services/ProfileService'
import { LockScreen } from './LockScreen'
import { Profile } from './Profile'

import './App.css'

export const App: React.FC = () => {
  const authService = useInstance(AuthService)
  const profileService = useInstance(ProfileService)
  const [ locked, setLocked ] = useState(true)

  useEffect(() => {
    const subscription = authService
      .observeLockState()
      .subscribe(setLocked)

    return () => {
      subscription.unsubscribe()
    }
  }, [authService])

  useEffect(() => {
    profileService.loadProfile()
  }, [profileService])

  return (
    <div className={`App ${locked && 'is-locked'}`}>
      {locked ? <LockScreen /> : <Profile />}
    </div>
  )
}
