import React, { useEffect, useState } from 'react'
import { useInstance } from '../hooks/useInstance'
import { IProfile, ProfileService } from '../services/ProfileService'

import './Profile.css'

export const Profile: React.FC = () => {
  const profileService = useInstance(ProfileService)
  const [ profile, setProfile ] = useState<IProfile | null>(null)

  useEffect(() => {
    const subscription = profileService
      .observeProfile()
      .subscribe(setProfile)

    return () => {
      subscription.unsubscribe()
    }
  }, [profileService])

  if (profile === null) {
    return null
  }

  return (
    <div className="Profile">
      <div className="Profile-hero">
        <img className="Profile-avatar" src={profile.avatar} />

        <div className="Profile-heroCopy">
          <div className="Profile-header">
            {profile.title}
          </div>

          <div className="Profile-strapline">
            {profile.strapline}
          </div>

          <div className="Profile-description">
            {profile.description}
          </div>
        </div>

        <div className="Profile-scrollHint" />
      </div>

      <ul className="Profile-skills">
        {profile.skills.map(skill => (
          <li className="Profile-skill" key={skill.name}>
            <div className="Profile-skillName">{skill.name}</div>
            <div className="Profile-skillProficiency" data-score={skill.proficiency} />
          </li>
        ))}
      </ul>

      <div className="Profile-sections">
        {profile.sections.map((section, i) => (
          <div className="Profile-section" data-type={section.type} key={i}>
            <div className="Profile-section-tagLine">{section.tagLine}</div>
            <div className="Profile-section-title">{section.title}</div>
            <div className="Profile-section-description">{section.description}</div>
            {section.url && <a href={section.url} target="_blank" className="Profile-section-url">{section.url}</a>}
          </div>
        ))}
      </div>

      <div className="Profile-footer">
        <div className="Profile-footerCopy">
          {profile.statement}
        </div>
      </div>
    </div>
  )
}
