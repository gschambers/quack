import CryptoJS from 'crypto-js'
import { injectable } from 'tsyringe'

interface EncryptionPayload {
  readonly data: string
}

@injectable()
export class CryptoUtils {
  public decrypt(payload: EncryptionPayload, passphrase: string): string {
    const result = CryptoJS.AES.decrypt(payload.data, passphrase).toString(CryptoJS.enc.Utf8)
    
    // None of the available encryption modes support MAC functions
    // so we need to reject false-positive decryptions. We could
    // alternatively create an HMAC of the encryption key
    if (result === '') {
      throw new Error('Invalid decryption key')
    }

    return result
  }
}
