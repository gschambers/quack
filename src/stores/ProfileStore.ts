import { Maybe, None } from 'monet'
import { BehaviorSubject, Observable, EMPTY, of } from 'rxjs'
import { switchMap } from 'rxjs/operators'
import { scoped, Lifecycle } from 'tsyringe'
import { IEncryptedPayload, IStore, TypeOf } from './types'

export interface ProfileState {
  readonly profile: Maybe<IEncryptedPayload>
}

@scoped(Lifecycle.ContainerScoped)
export class ProfileStore implements IStore<ProfileState> {
  private state = new BehaviorSubject({
    profile: None(),
  })

  public valueOf<K extends keyof ProfileState>(key: K) {
    return this.state.pipe(
      switchMap(values => {
        const value = values[key]

        if (Maybe.isInstance(value)) {
          return value.cata<Observable<TypeOf<ProfileState[K]>>>(() => EMPTY, of)
        }

        return value
      }),
    )
  }

  public setValue<K extends keyof ProfileState>(key: K, value: ProfileState[K]) {
    const currentState = this.state.getValue()
    const nextState = {
      ...currentState,
      [key]: value,
    }

    this.state.next(nextState)
  }
}
