import { Maybe } from 'monet'
import { Observable } from 'rxjs'

export type TypeOf<T> = T extends Maybe<infer U> ? U : T

export interface IReadonlyStore<T> {
  readonly valueOf: <K extends keyof T>(key: K) => Observable<TypeOf<T[K]>>
}

export interface IStore<T> extends IReadonlyStore<T> {
  readonly setValue: <K extends keyof T>(key: K, value: T[K]) => void
}

export interface IEncryptedPayload {
  readonly data: string
}
