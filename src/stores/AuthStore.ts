import { BehaviorSubject } from 'rxjs'
import { map } from 'rxjs/operators'
import { scoped, Lifecycle } from 'tsyringe'
import { IStore } from './types'

export interface AuthState {
  readonly locked: boolean
  readonly passphrase: string
}

@scoped(Lifecycle.ContainerScoped)
export class AuthStore implements IStore<AuthState> {
  private state = new BehaviorSubject<AuthState>({
    locked: true,
    passphrase: '',
  })

  public valueOf<K extends keyof AuthState>(key: K) {
    return this.state.pipe(
      map(values => values[key]),
    )
  }

  public setValue<K extends keyof AuthState>(key: K, value: AuthState[K]) {
    this.state.next({
      ...this.state.getValue(),
      [key]: value,
    })
  }
}
