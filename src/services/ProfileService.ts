import axios from 'axios'
import { Some } from 'monet'
import { map, withLatestFrom } from 'rxjs/operators'
import { scoped, Lifecycle } from 'tsyringe'
import { AuthStore } from '../stores/AuthStore'
import { ProfileStore } from '../stores/ProfileStore'
import { IEncryptedPayload } from '../stores/types'
import { CryptoUtils } from '../utils/CryptoUtils'

interface IProfileSkill {
  readonly name: string
  readonly proficiency: number
}

interface IProfileSection {
  readonly type: 'Project' | 'Employment'
  readonly tagLine: string
  readonly title: string
  readonly description: string
  readonly url?: string
}

export interface IProfile {
  readonly avatar: string
  readonly title: string
  readonly strapline: string
  readonly description: string
  readonly skills: IProfileSkill[]
  readonly sections: IProfileSection[]
  readonly statement: string
}

@scoped(Lifecycle.ContainerScoped)
export class ProfileService {
  constructor(
    private readonly authStore: AuthStore,
    private readonly profileStore: ProfileStore,
    private readonly cryptoUtils: CryptoUtils) {}

  public loadProfile() {
    return axios.get<IEncryptedPayload>('/vault/profile.enc.json')
      .then(res => this.profileStore.setValue('profile', Some(res.data)))
  }

  public observeProfile() {
    return this.authStore.valueOf('passphrase')
      .pipe(
        withLatestFrom(this.profileStore.valueOf('profile')),
        map(([passphrase, profile]) => this.cryptoUtils.decrypt(profile, passphrase)),
        map<string, IProfile>(data => JSON.parse(data)),
      )
  }
}
