import { Observable } from 'rxjs'
import { scoped, Lifecycle } from 'tsyringe'
import { AuthStore } from '../stores/AuthStore'

@scoped(Lifecycle.ContainerScoped)
export class AuthService {
  constructor(private readonly authStore: AuthStore) {}
  
  public observeLockState(): Observable<boolean> {
    return this.authStore.valueOf('locked')
  }

  public lock(): void {
    this.authStore.setValue('locked', true)
  }

  public unlock(): void {
    this.authStore.setValue('locked', false)
  }

  public setPassphrase(passphrase: string): void {
    this.authStore.setValue('passphrase', passphrase)
  }
}
