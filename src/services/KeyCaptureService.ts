import { fromEvent, Observable } from 'rxjs'
import { distinctUntilChanged, scan } from 'rxjs/operators'
import { scoped, Lifecycle } from 'tsyringe'

const VALID_CHAR = /^[A-Za-z]$/
const MAX_LENGTH = 16

@scoped(Lifecycle.ContainerScoped)
export class KeyCaptureService {
  public observeKeySequence(): Observable<string> {
    return fromEvent<KeyboardEvent>(document, 'keydown').pipe(
      scan((sequence, evt) => {
        const key = evt.key

        if (evt.ctrlKey || evt.altKey || evt.metaKey) {
          return sequence
        }

        if (key === 'Backspace') {
          return sequence.slice(0, -1)
        }

        if (sequence.length === MAX_LENGTH) {
          return sequence
        }

        if (evt.repeat) {
          return sequence
        }

        if (VALID_CHAR.test(key)) {
          return sequence + key.toLowerCase()
        }

        return sequence
      }, ''),
      distinctUntilChanged(),
    )
  }
}

