import { useContext } from 'react'
import { InjectionToken } from 'tsyringe'
import { ContainerContext } from '../ContainerContext'

export function useInstance<T>(token: InjectionToken<T>): T {
  const container = useContext(ContainerContext)
  return container.resolve(token)
}
